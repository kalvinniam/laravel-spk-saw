<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('index');

Auth::routes();

Route::group(['prefix' => 'login'], function () {
	Route::get('/', 'Auth\LoginController@showLoginForm')->name("login");
	Route::post('/', 'Auth\LoginController@login')->name("login.submit");
});
Route::get('logout', ['as'=> 'logout', 'uses' => 'Auth\LoginController@logout']);

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});

Route::group(['prefix' => 'spk', 'middleware' => ['auth','auth.userinfo']], function () {
	
	Route::group(['prefix' => 'alternatif', 'middleware'=> ['auth.spk', 'auth.kriteria']], function () {
		
		// permission: mahasiswa, dosen, admin
		Route::get('/', ['as' => 'alternatif.index', 'uses' => 'AlternatifController@index']);
		
		// permission: dosen, admin
		Route::group(['middleware' => 'IsMahasiswa'], function () {
			Route::post('/', ['as' => 'alternatif.store', 'uses' => 'AlternatifController@store']);
			Route::get('/tambah', ['as' => 'alternatif.create', 'uses' => 'AlternatifController@create']);
			Route::get('/edit/{id}', ['as' => 'alternatif.edit', 'uses' => 'AlternatifController@edit']);
			Route::post('/edit/{id}', ['as' => 'alternatif.update', 'uses' => 'AlternatifController@update']);
			Route::delete('/{id}', ['as' => 'alternatif.destroy', 'uses' => 'AlternatifController@destroy']);
			// Route::post('/import', ['as' => 'alternatif.import', 'uses' => 'AlternatifController@import']);
		});
	});
	
	// permission: mahasiswa, dosen, admin
	Route::get('/', ['as'=> 'spk.index', 'uses' => 'SpkGroupController@index']);
	Route::get('select/{id}', ['as' => 'spk.select', 'uses' => 'SpkGroupController@select']);
	// permission: dosen, admin
	Route::group(['middleware' => 'IsMahasiswa'], function () {
		Route::get('/edit/{id}', ['as' => 'spk.edit', 'uses' => 'SpkGroupController@edit']);
		Route::post('/edit/{id}', ['as' => 'spk.update', 'uses' => 'SpkGroupController@update']);
		Route::post('/', ['as' => 'spk.store', 'uses' => 'SpkGroupController@store']);
		Route::delete('/{id}', ['as' => 'spk.destroy', 'uses' => 'SpkGroupController@destroy']);
	});
	

	Route::group(['prefix' => 'kriteria', 'middleware'=> 'auth.spk'], function () {
		
		// permission: mahasiswa, dosen, admin
		Route::get('/', ['as' => 'kriteria.index', 'uses' => 'KriteriaController@index']);
		// permission: dosen, admin
		Route::group(['middleware' => 'IsMahasiswa'], function () {
			Route::post('/', ['as' => 'kriteria.store', 'uses' => 'KriteriaController@store']);
			Route::get('/tambah', ['as' => 'kriteria.create', 'uses' => 'KriteriaController@create']);
			Route::get('/edit/{id}', ['as' => 'kriteria.edit', 'uses' => 'KriteriaController@edit']);
			Route::put('/{id}', ['as' => 'kriteria.update', 'uses' => 'KriteriaController@update']);
			Route::delete('/{id}', ['as' => 'kriteria.destroy', 'uses' => 'KriteriaController@destroy']);
		});
	});

	Route::group(['prefix' => 'skala', 'middleware'=> 'auth.spk'], function () {
		// permission: mahasiswa, dosen, admin
		Route::get('/', ['as' => 'skala.index', 'uses' => 'SkalaController@index']);
		// permission: dosen, admin
		Route::group(['middleware' => 'IsMahasiswa'], function () {
			Route::post('/kriteria/{kriteria_id}', ['as' => 'skala.store', 'uses' => 'SkalaController@store']);
			Route::get('/kriteria/{kriteria_id}/tambah/', ['as' => 'skala.create', 'uses' => 'SkalaController@create']);
			Route::get('/{skala_id}/kriteria/{kriteria_id}/edit', ['as' => 'skala.edit', 'uses' => 'SkalaController@edit']);
			Route::put('/{skala_id}/kriteria/{kriteria_id}', ['as' => 'skala.update', 'uses' => 'SkalaController@update']);
			Route::delete('/{skala_id}/kriteria/{kriteria_id}', ['as' => 'skala.destroy', 'uses' => 'SkalaController@destroy']);
		});
	});

	Route::group(['prefix' => 'perhitungan', 'middleware'=> 'auth.spk'], function () {
		Route::group(['middleware' => ['IsMahasiswa']], function () {
			Route::get('/', ['as' => 'perhitungan.index', 'uses' => 'PerhitunganController@index']); //metode SAW
		});
		Route::get('/akhir', ['as' => 'perhitungan.akhir', 'uses' => 'PerhitunganController@akhir']); //metode Borda
	});
	
});
