<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @property int $id
 * @property int $role_id
 * @property string $nama
 * @property string $email
 * @property string $email_verified_at
 * @property string $password
 * @property string $remember_token
 * @property string $created_at
 * @property string $updated_at
 * @property Role $role
 * @property Penilaian[] $penilaians
 * @property SpkGroup[] $spkGroups
 */
class User extends Authenticatable
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    use Notifiable;
    protected $table = 'user';
    /**
     * @var array
     */
    protected $fillable = ['role_id', 'nama', 'email', 'email_verified_at', 'password', 'remember_token', 'created_at', 'updated_at'];
    protected $hidden = [
        'password', 'remember_token',
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo('App\Role');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function penilaians()
    {
        return $this->hasMany('App\Penilaian');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function spkGroups()
    {
        return $this->hasMany('App\SpkGroup');
    }
}
