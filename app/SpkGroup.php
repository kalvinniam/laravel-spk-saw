<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property string $kode
 * @property string $judul
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @property Bobot[] $bobots
 */
class SpkGroup extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'spk_group';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'kode', 'judul', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bobots()
    {
        return $this->hasMany('App\Bobot');
    }
}
