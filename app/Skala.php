<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $kriteria_id
 * @property string $nama_skala
 * @property string $keterangan
 * @property int $value
 * @property string $created_at
 * @property string $updated_at
 * @property Kriterium $kriterium
 * @property Penilaian[] $penilaians
 */
class Skala extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'skala';

    /**
     * @var array
     */
    protected $fillable = ['kriteria_id', 'nama_skala', 'keterangan', 'value', 'created_at', 'updated_at'];
    protected $hidden = [
        'created_at', 'updated_at',
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kriteria()
    {
        return $this->belongsTo('App\Kriteria', 'kriteria_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function penilaians()
    {
        return $this->hasMany('App\Penilaian');
    }
}
