<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $user_id
 * @property int $alternatif_id
 * @property int $rank
 * @property string $created_at
 * @property string $updated_at
 * @property Alternatif $alternatif
 * @property User $user
 */
class Borda extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'borda';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'alternatif_id', 'rank', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function alternatif()
    {
        return $this->belongsTo('App\Alternatif');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
