<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $kode_alternatif
 * @property string $nama_alternatif
 * @property string $created_at
 * @property string $updated_at
 * @property Penilaian[] $penilaians
 */
class Alternatif extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'alternatif';

    /**
     * @var array
     */
    protected $fillable = ['kode_alternatif', 'nama_alternatif', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function penilaians()
    {
        return $this->hasMany('App\Penilaian');
    }
}
