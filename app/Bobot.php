<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $spk_group_id
 * @property int $kriteria_id
 * @property float $persentase
 * @property string $jenis
 * @property string $created_at
 * @property string $updated_at
 * @property Kriterium $kriterium
 * @property SpkGroup $spkGroup
 */
class Bobot extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'bobot';

    /**
     * @var array
     */
    protected $fillable = ['spk_group_id', 'kriteria_id', 'persentase', 'jenis', 'created_at', 'updated_at'];
    protected $hidden = [
        'created_at', 'updated_at',
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kriteria()
    {
        return $this->belongsTo('App\Kriteria', 'kriteria_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function spkGroup()
    {
        return $this->belongsTo('App\SpkGroup');
    }
}
