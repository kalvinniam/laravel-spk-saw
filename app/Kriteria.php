<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $kode_kriteria
 * @property string $nama_kriteria
 * @property string $created_at
 * @property string $updated_at
 * @property Bobot[] $bobots
 * @property Penilaian[] $penilaians
 * @property Skala[] $skalas
 */
class Kriteria extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'kriteria';

    /**
     * @var array
     */
    protected $fillable = ['kode_kriteria', 'nama_kriteria', 'created_at', 'updated_at'];
    protected $hidden = [
        'created_at', 'updated_at',
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bobots()
    {
        return $this->hasMany('App\Bobot', 'kriteria_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function penilaians()
    {
        return $this->hasMany('App\Penilaian', 'kriteria_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function skalas()
    {
        return $this->hasMany('App\Skala', 'kriteria_id');
    }
}
