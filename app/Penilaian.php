<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;

/**
 * @property int $id
 * @property int $user_id
 * @property int $skala_id
 * @property int $alternatif_id
 * @property int $kriteria_id
 * @property float $nilai
 * @property string $created_at
 * @property string $updated_at
 * @property Alternatif $alternatif
 * @property Kriterium $kriterium
 * @property Skala $skala
 * @property User $user
 */
class Penilaian extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'penilaian';

    /**
     * @var array
     */
    protected $fillable = ['user_id', 'skala_id', 'alternatif_id', 'kriteria_id', 'nilai', 'created_at', 'updated_at'];
    protected $hidden = [
        'created_at', 'updated_at',
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function alternatif()
    {
        return $this->belongsTo('App\Alternatif');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kriterias()
    {
        return $this->belongsTo('App\Kriteria', 'kriteria_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function skala()
    {
        return $this->belongsTo('App\Skala');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
