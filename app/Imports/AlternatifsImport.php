<?php

namespace App\Imports;

use App\Alternatif;
use App\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
HeadingRowFormatter::default('none');

class AlternatifsImport implements ToModel,  WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if(!empty($row['Kode Alternatif'])){

            $cek = Alternatif::where('kode_alternatif', $row['Kode Alternatif'])->count();

            if($cek == 0){ //jika belum ada maka tambahkan
                
                if($row['Kode Alternatif'] == ""){
                    $errMessage = 'Kolom kode alternatif tidak boleh kosong.';
                    throw new \Exception($errMessage);
                }
                if(strlen($row['Kode Alternatif']) > 10){
                    $errMessage = 'Kode alternatif maksimal 10 huruf/chars.';
                    throw new \Exception($errMessage);
                }

                if($row['Nama Alternatif'] == ""){
                    $errMessage = 'Kolom nama alternatif tidak boleh kosong.';
                    throw new \Exception($errMessage);
                }
                if(strlen($row['Nama Alternatif']) > 50){
                    $errMessage = 'Nama alternatif maksimal 50 huruf/chars.';
                    throw new \Exception($errMessage);
                }
                
                $alternatif = new Alternatif([
                    'kode_alternatif'     => $row['Kode Alternatif'],
                    'nama_alternatif'     => $row['Nama Alternatif'],
                ]);

                $kriterias = Kriteria::all();
                $users = User::all();
                for ($i=0; $i <count($users); $i++) { 
                    $penilaians = [];
                    for ($j=0; $j <count($kriterias); $j++) { 
                        $penilaians[] = [
                            'kriteria_id' => $kriterias[$j],
                            'alternatif_id' => $alternatif->id,
                            'nilai' => 0,
                            'user_id' => $users[$i]->id,
                            'skala_id' => 0, //???????? THERE IS NO WAY TO DO THIS WAY. TOO COMPLEX FOR USE AND FOR DEVELOPER,
                            'created_at' => now(),
                            'updated_at' => now()
                        ];
                    }
                    DB::table('penilaian')->insert($penilaians);
                }
                
                return $alternatif;
            }else{
                if($row['Kode Alternatif'] == ""){
                    $errMessage = 'Kolom kode alternatif tidak boleh kosong.';
                    throw new \Exception($errMessage);
                }
                if(strlen($row['Kode Alternatif']) > 10){
                    $errMessage = 'Kode alternatif maksimal 10 huruf/chars.';
                    throw new \Exception($errMessage);
                }

                if($row['Nama Alternatif'] == ""){
                    $errMessage = 'Kolom nama alternatif tidak boleh kosong.';
                    throw new \Exception($errMessage);
                }
                if(strlen($row['Nama Alternatif']) > 50){
                    $errMessage = 'Nama alternatif maksimal 50 huruf/chars.';
                    throw new \Exception($errMessage);
                }

                $arr = [
                    'kode_alternatif'   => $row['Kode Alternatif'],
                    'nama_alternatif'   => $row['Nama Alternatif'],
                ];

                if(!empty($arr)){
                    Alternatif::where('kode_alternatif', $row['Kode Alternatif'])
                    ->update($arr);
                }
            }
        }
    }
}
