<?php

namespace App\Http\Controllers;

use App\Alternatif;
use Illuminate\Http\Request;
use App\Kriteria;
use App\Bobot;
use App\SpkGroup;
use App\Skala;
use View;
use Validator;
use Session;
use DB;

class KriteriaController extends Controller
{
    public function index()
    {
        $kriterias = Bobot::where('spk_group_id', Session::get('currentSpkGroup')->id)
        ->with('kriteria')
        ->paginate(15);
        $data =[
            'kriterias' => $kriterias
        ];
        return View::make('kriterias.index', $data);
    }

    public function create()
    {
        return view('kriterias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "kode_kriteria" => "required|string|unique:kriteria,kode_kriteria",
            "nama_kriteria" => "required|string",
            "bobot_kriteria" => "required|numeric"
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                            ->withInput($request->input())
                            ->withErrors($validator);
        }

        $kriteria = Kriteria::create([
            "nama_kriteria" => $request->nama_kriteria,
            "kode_kriteria" => $request->kode_kriteria
        ]);

        $bobot = Bobot::create([
            "jenis" => $request->jenis_kriteria,
            "persentase" => doubleval($request->bobot_kriteria),
            "kriteria_id" => $kriteria->id,
            "spk_group_id" => Session::get('currentSpkGroup')->id
        ]);

        // default skala initialization for new kriteria
        DB::table('skala')->insert([
            [
                'nama_skala' => 'Sangat Baik',
                'value' => 90,
                'keterangan' => '90-100',
                'kriteria_id' => $kriteria->id,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'nama_skala' => 'Baik',
                'value' => 80,
                'keterangan' => '80-89',
                'kriteria_id' => $kriteria->id,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'nama_skala' => 'Cukup',
                'value' => 70,
                'keterangan' => '70-79',
                'kriteria_id' => $kriteria->id,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'nama_skala' => 'Kurang',
                'value' => 60,
                'keterangan' => '60-69',
                'kriteria_id' => $kriteria->id,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'nama_skala' => 'Sangat Kurang',
                'value' => 50,
                'keterangan' => '50-59',
                'kriteria_id' => $kriteria->id,
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
        // return $skalasIds;
        // $id = DB::table('users')->insertGetId(
        //     ['email' => 'john@example.com', 'votes' => 0]
        // );
        $skala = Skala::where('kriteria_id', $kriteria->id)->first();
        // inisialisai penilaian alternatif untuk kriteria baru
        $alternatifs = Alternatif::all();
        // $kriteria->id
        
        for ($i=0; $i <count($alternatifs); $i++) { 
            DB::table('penilaian')->insert([
                'kriteria_id' => $kriteria->id,
                'alternatif_id' => $alternatifs[$i]->id,
                'spk_group_id'  => Session::get('currentSpkGroup')->id,
                'nilai' => 0,
                'user_id' => Session::get('userInfo')->id,
                'skala_id' => $skala->id,
                'created_at' => now(),
                'updated_at' => now()
            ]);
        }
        
        $spkGroups = SpkGroup::all();
        foreach ($spkGroups as $key => $spkGroup) {
            if($spkGroup->id != Session::get('currentSpkGroup')->id){
                $bobot = Bobot::create([
                    "jenis" => 'BENEFIT',
                    "persentase" => 0,
                    "kriteria_id" => $kriteria->id,
                    "spk_group_id" => $spkGroup->id
                ]);
            }
        }
        
        return redirect()->route('kriteria.index')
        ->withStatus('Kriteria baru berhasil ditambahkan. Sekarang Anda bisa memperbarui pembobotan kriteria dan penilaian alternatif untuk semua kelompok perhitungan yang ada.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Kriteria  $kriteria
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Bobot::where('id', $id)
                            ->with('kriteria')->first();
        if (!$data) {
            return redirect()->route('kriteria.index')->withStatus('Kriteria tidak ditemukan.');
        }
        $data =[
            'data' => $data
        ];
        return view('kriterias.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kriteria  $kriteria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $key)
    {
        $validator = Validator::make($request->all(), [
            "kode_kriteria" => "required|string",
            "nama_kriteria" => "required|string",
            "bobot_kriteria" => "required|numeric"
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                            ->withInput($request->input())
                            ->withErrors($validator);
        }

        $bobot = Bobot::find($key);
        if (!$bobot) {
            return redirect()->back()->withStatus('Bobot kriteria tidak ditemukan.');
        }
        $kriteria = Kriteria::find($bobot->kriteria_id);
        if (!$kriteria) {
            return redirect()->back()->withStatus('Kriteria tidak ditemukan.');
        }
        
        $bobot->persentase = $request->bobot_kriteria;
        $bobot->jenis = $request->jenis_kriteria;
        $kriteria->kode_kriteria = $request->kode_kriteria;
        $kriteria->nama_kriteria = $request->nama_kriteria;
        $bobot->save();
        $kriteria->save();
        return redirect()->route('kriteria.index')->withStatus('Kriteria berhasil diperbarui.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kriteria  $kriteria
     * @return \Illuminate\Http\Response
     */
    public function destroy($key)
    {
        $kriteria = Kriteria::find($key);
        if (!$kriteria) {
            return redirect()->back()->withStatus('Kriteria tidak ditemukan.');
        }
        $kriteria->delete();
        return redirect()->route('kriteria.index')->withStatus('Kriteria berhasil dihapus.');
    }
}
