<?php

namespace App\Http\Controllers;

use App\Alternatif;
use Illuminate\Http\Request;
use App\Skala;
use App\Bobot;
use App\SpkGroup;
use App\Kriteria;
use View;
use Validator;
use Session;
use DB;

class SkalaController extends Controller
{
    public function index()
    {
        $skalas = Kriteria::with('skalas')
                        ->paginate(15);
        $data =[
            'kriterias' => $skalas
        ];
        // return $data;
        return View::make('skalas.index', $data);
    }

    public function create($kriteria_id)
    {
        $kriteria = Kriteria::find($kriteria_id);

        if (!$kriteria) {
            return redirect()->back()->withStatus('Kriteria tidak ditemukan.');
        }
        
        $data = [
            'kriteria' => $kriteria
        ];
        // return $data;
        return view('skalas.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            "nama_skala" => "required|string",
            "keterangan" => "required|string",
            "value" => "required|numeric"
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                            ->withInput($request->input())
                            ->withErrors($validator);
        }

        $kriteria = Kriteria::find($id);
        if (!$kriteria) {
            return redirect()->back()->withStatus('Kriteria tidak ditemukan.');
        }
        
        $skala = Skala::create([
            "kriteria_id" => $id,
            "nama_skala" => $request->nama_skala,
            "keterangan" => $request->keterangan,
            "value" => $request->value
        ]);

        Session::flash('kriteria_id', $kriteria->id);
        return redirect()->route('skala.index')
        ->withStatus('Skala baru berhasil ditambahkan. Sekarang Anda bisa memperbarui penilaian alternatif terhadap kriteria'. $kriteria->nama_kriteria .'untuk semua kelompok perhitungan yang ada.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Skala  $skala
     * @return \Illuminate\Http\Response
     */
    public function edit($skala_id, $kriteria_id)
    {
        $skala = Skala::where('id', $skala_id)->with(['kriteria' => function($kriteria) use ($kriteria_id){
            $kriteria->where('id', $kriteria_id);
        }])->first();

        if (!$skala) {
            Session::flash('kriteria_id', $kriteria_id);
            return redirect()->back()->withStatus('Skala tidak ditemukan.');
        }

        $data = [
            'skala' => $skala
        ];
        // return $data;
        return View::make('skalas.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Skala  $skala
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $skala_id, $kriteria_id)
    {
        $validator = Validator::make($request->all(), [
            "nama_skala" => "required|string",
            "keterangan" => "required|string",
            "value" => "required|numeric"
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                            ->withInput($request->input())
                            ->withErrors($validator);
        }

        $kriteria = Kriteria::find($kriteria_id);
        if (!$kriteria) {
            Session::flash('kriteria_id', $kriteria->id);
            return redirect()->back()->withStatus('Kriteria tidak ditemukan.');
        }
        
        $skala = Skala::find($skala_id);
        $skala->kriteria_id = $kriteria_id;
        $skala->nama_skala = $request->nama_skala;
        $skala->keterangan = $request->keterangan;
        $skala->value = $request->value;

        $skala->save();
        Session::flash('kriteria_id', $kriteria->id);
        return redirect()->route('skala.index')
        ->withStatus('Skala penilaian untuk kriteria \''.$kriteria->nama_kriteria.'\' berhasil diperbarui.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Skala  $skala
     * @return \Illuminate\Http\Response
     */
    public function destroy($skala_id, $kriteria_id)
    {
        $kriteria = Kriteria::find($kriteria_id);
        if (!$kriteria) {
            Session::flash('kriteria_id', $kriteria->id);
            return redirect()->back()->withStatus('Kriteria tidak ditemukan.');
        }
        
        $skala = Skala::find($skala_id);
        if (!$skala) {
            return redirect()->back()->withStatus('Skala tidak ditemukan.');
        }
        $skala->delete();
        Session::flash('kriteria_id', $kriteria_id);
        return redirect()->route('skala.index')->withStatus('Skala berhasil dihapus.');
    }
}
