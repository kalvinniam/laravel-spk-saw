<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SpkGroup;
use View;
use Validator;
use Session;
use App\Bobot;
use App\Kriteria;
use App\Alternatif;
use App\Skala;
use App\User;
use DB;

class SpkGroupController extends Controller
{
    public function index()
    {
        $spkGroups = SpkGroup::paginate(15);
        $data =[
            'spkGroups' => $spkGroups
        ];
        // return $data;
        return View::make('dashboard', $data);
    }

    public function create()
    {
        return view('spkGroups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function select($id){
        $spkGroup = SpkGroup::where('id', $id)->first();
        if($spkGroup){
            Session::put ('currentSpkGroup', $spkGroup);
            return "ok";
        }
    }
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            "user_id" => "required|integer",
            "kode" => "required|string|unique:spk_group,kode",
            "judul" => "required|string"
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)
                        ->withInput();
        }

        $spkGroup = SpkGroup::create([
            "user_id" => $request->user_id,
            "kode" => $request->kode,
            "judul" => $request->judul
        ]);

        $kriterias = Kriteria::all();

        if(Session::get('currentSpkGroup')){
            $existBobots = Bobot::where('spk_group_id', Session::get('currentSpkGroup')->id)->get();
        }

        foreach ($kriterias as $key => $kriteria) {
            $bobot = Bobot::create([
                "jenis" => isset($existBobots) ? $existBobots[$key]->jenis: 'BENEFIT',
                "persentase" => isset($existBobots) ? $existBobots[$key]->persentase: 0,
                "kriteria_id" => $kriteria->id,
                "spk_group_id" => $spkGroup->id
            ]);
        }

        $alternatifs = Alternatif::all();
        $users = User::whereIn('role_id', [1, 3])->get();
        foreach ($users as $key => $user) {
            foreach ($alternatifs as $key => $alternatif) {
                    // tambahkan baris baru di tabel borda
                    $borda = [];
                    $borda[] = [
                        'rank'          => 0,
                        'user_id'       => $user->id,
                        'alternatif_id' => $alternatif->id,
                        'spk_group_id'  => $spkGroup->id,
                        'created_at'    => now(),
                        'updated_at'    => now()
                    ];
                    DB::table('borda')->insert($borda);
            }
        }
        foreach ($kriterias as $key => $kriteria) {
            $skala = Skala::where('kriteria_id', $kriteria->id)->first();
            for ($i=0; $i <count($alternatifs); $i++) { 
                DB::table('penilaian')->insert([
                    'kriteria_id' => $kriteria->id,
                    'alternatif_id' => $alternatifs[$i]->id,
                    'spk_group_id'  => Session::get('currentSpkGroup')->id,
                    'nilai' => 0,
                    'user_id' => Session::get('userInfo')->id,
                    'skala_id' => $skala->id,
                    'created_at' => now(),
                    'updated_at' => now()
                ]);
            }
        }

        if($spkGroup){
            Session::put ('currentSpkGroup', $spkGroup);
        }
        return redirect()->route('kriteria.index')->withStatus('Sekarang Anda bisa memperbarui pembobotan kriteria dan untuk kelompok perhitungan \''.$spkGroup->judul.'\'');
        // return response()->json([
        //     "pesan" => "SpkGroup berhasil ditambahkan!",
        //     "spkGroup" => $spkGroup
        // ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SpkGroup  $spkGroup
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $spkGroup = SpkGroup::find($id);
        return $spkGroup;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SpkGroup  $spkGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $key)
    {
        $spkGroup = SpkGroup::find($key);
        if (!$spkGroup) {
            return response()->json([
                "pesan" => "SpkGroup tidak ditemukan!",
                "key" => $key
            ], 404);
        }
        
        $validator = Validator::make($request->all(), [
            "kode" => "required|string",
            "judul" => "required|string",
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $spkGroup->kode = $request->kode;
        $spkGroup->judul = $request->judul;
        $spkGroup->save();
        $request->session()->flash('status', 'Kelompok perhitungan berhasil diperbarui.');
        return response()->json([
            "redirect" => route('spk.index')
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SpkGroup  $spkGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy($key)
    {
        $spkGroup = SpkGroup::find($key);
        if (!$spkGroup) {
            return redirect()->back()->withStatus('Kelompok perhitungan tidak ditemukan.');
        }
        $spkGroup->delete();
        $spkGroup = SpkGroup::latest()->first();
        if($spkGroup){
            Session::put ('currentSpkGroup', $spkGroup);
            return redirect()->route('spk.index')->withStatus('Kelompok perhitungan berhasil dihapus.');
        }else{
            Session::forget('currentSpkGroup');
            Session::flash('type', 'secondary');
            Session::flash('notification', 'Kelompok perhitungan berhasil dikosongkan.');
            return redirect()->route('spk.index')->withStatus('Kelompok perhitungan berhasil dihapus.');
        }
        // $spkGroup = SpkGroup::find($key);
        // if (!$spkGroup) {
        //     return response()->json([
        //         "pesan" => "SpkGroup tidak ditemukan!",
        //         "key" => $key
        //     ], 404);
        // }
    }
}