<?php

namespace App\Http\Controllers;

use App\Alternatif;
use Illuminate\Http\Request;
use DB;
use App\Kriteria;
use App\Penilaian;
use App\Bobot;
use App\User;
use App\Borda;
use View;
use Session;

class PerhitunganController extends Controller
{
    public function index(Request $request){
        if($request->has('user')) {
            $user_id = $request->user;
            $userRole = User::findOrFail($user_id);
            if($userRole->role_id != 1 && $userRole->role_id != 3 ){
                $user_id = 1;
            }
        }else{
            $user_id = 1;
        }

        $users = User::whereIn('role_id', [1, 3])->with('role')->get();
        $pembobotans = Bobot::where('spk_group_id', Session::get('currentSpkGroup')->id)
                            ->with('kriteria')
                            ->get();
        $datas = Alternatif::with(['penilaians' => function ($penilaian) use ($user_id){
                            $penilaian->where([
                                ['user_id', $user_id],
                                ['spk_group_id', Session::get('currentSpkGroup')->id]
                            ])->with('skala');
                        }])->get();
        $matriksNorm = [];
        $matriksAwal = [];
        $mins = [];
        $maxs = [];

        for ($i=0; $i < count($datas); $i++) { 
            for ($j=0; $j < count($pembobotans); $j++) { 
                $matriksAwal[$j][$i] = $datas[$i]->penilaians[$j]->skala->value;
            }
        }

        for ($j=0; $j < count($matriksAwal); $j++) { 
            $mins[$j][] = min($matriksAwal[$j]);
            $maxs[$j][] = max($matriksAwal[$j]);
        }

        for ($i=0; $i < count($datas); $i++) { 
            for ($j=0; $j < count($pembobotans); $j++) { 
                if($pembobotans[$j]->jenis == 'BENEFIT'){
                    $matriksNorm[$i][$j] = $matriksAwal[$j][$i] / max($matriksAwal[$j]);
                }else{
                    $matriksNorm[$i][$j] = min($matriksAwal[$j]) / $matriksAwal[$j][$i];
                }
            }
        }
        // saving to db
        for ($i=0; $i < count($datas); $i++) { 
            for ($j=0; $j < count($datas[$i]->penilaians); $j++) { 
                DB::table('penilaian')
                            ->where('id', $datas[$i]->penilaians[$j]->id)
                            ->update([
                                'nilai' => $matriksNorm[$i][$j]
                            ]);
            }
        }
        
        // perangkingan
        $rangkings = [];
        $hasilPerhitungan = [];
        for ($i=0; $i < count($matriksNorm); $i++) {  //3
            $perkalian = [];
            // perhitungan poin
            for ($j=0; $j < count($matriksNorm[$i]); $j++) { 
                $perkalian[] = $matriksNorm[$i][$j] * $pembobotans[$j]->persentase;
            }
            $rangkings[$i] = array_sum($perkalian);
            $hasilPerhitungan[$i] = new \stdClass();
            $hasilPerhitungan[$i]->id =  $datas[$i]->id;
            $hasilPerhitungan[$i]->nama_alternatif =  $datas[$i]->nama_alternatif;
            $hasilPerhitungan[$i]->kode_alternatif =  $datas[$i]->kode_alternatif;
            $hasilPerhitungan[$i]->poin =  $rangkings[$i];
        }

        usort($hasilPerhitungan, function($a, $b){
            return strcmp($b->poin, $a->poin);
        });
         // tambahkan baris baru di tabel borda
        for ($i=0; $i < count($hasilPerhitungan); $i++) { 
            DB::table('borda')
                        ->where([
                                ['user_id', '=', Session::get('userInfo')->id],
                                ['alternatif_id', '=', $hasilPerhitungan[$i]->id ],
                                ['spk_group_id', Session::get('currentSpkGroup')->id]
                            ])
                        ->update([
                            'rank'          => $i + 1,
                            'updated_at'    => now()
                        ]);
        }
        
        $data = [
            'perhitungan_user' => $user_id,
            'users'            => $users,
            'pembobotans'      => $pembobotans,
            'datas'            => $datas,
            'matriksAwal'      => $matriksAwal,
            'mins'             => $mins,
            'maxs'             => $maxs,
            'matriksNorm'      => $matriksNorm,
            'rangkings'        => $hasilPerhitungan,
        ];
        DB::table('spk_group')
                        ->where('id', Session::get('currentSpkGroup')->id)
                        ->update([
                            'updated_at'    => now()
                        ]);
        // return $data;
        return View::make('perhitungans.index', $data);
    }
    
    public function akhir(){
        // BORDA ====
        $borda = Borda::all();
        $barisBorda = [];
        $users = User::whereIn('role_id', [1, 3])->get();
        for ($i=0; $i < count($users); $i++) { 
            $barisBorda[$i] = DB::table('borda')
            ->select('alternatif.id', 'alternatif.kode_alternatif', 'alternatif.nama_alternatif', 'rank')
            ->join('alternatif', 'borda.alternatif_id', '=', 'alternatif.id')
            ->where([
                ['user_id', $users[$i]->id],
                ['spk_group_id', Session::get('currentSpkGroup')->id],
                ])
            ->groupBy('alternatif.id')
            ->get();
            // DB::raw('
            // SELECT alternatif.id, alternatif.nama_alternatif, alternatif.kode_alternatif, SUM(rank) as jumlah
            // FROM borda 
            // INNER JOIN alternatif ON borda.alternatif_id=alternatif.id
            // where user_id = '. $users[$i]->id. 'GROUP BY alternatif_id;');
        }
        $ranking = [];
        $alternatifs = Alternatif::all();
        $poinRanking = [];
        for ($i=count($alternatifs); $i > 0; $i--) { 
            $poinRanking[] = $i-1;
        }
        $skor = [];
        for ($i=0; $i < count($alternatifs); $i++) { 
            $skor[$i] = $alternatifs[$i];
            $skor[$i]->skor = 0;
            for ($j=0; $j < count($barisBorda); $j++) { 
                $ranking[$j][$i] = $barisBorda[$j][$i]->rank;
                if($barisBorda[$j][$i]->rank - 1 >= 0){
                    $skor[$i]->skor = $skor[$i]->skor + $poinRanking[$barisBorda[$j][$i]->rank - 1];
                }
            }
        }

        usort($skor, function($a, $b){
            return strcmp($b->skor, $a->skor);
        });
        $data = [
            'hasils' => $skor
        ];
        // return $data;
        return View::make('perhitungans.akhir', $data);
    }
}
