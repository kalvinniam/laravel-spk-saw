<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Alternatif;
use App\Skala;
use App\Kriteria;
use App\Penilaian;
use App\User;
use View;
use Validator;
use DB;
use Session;
use Excel;
use App\Imports\AlternatifsImport;
use Exception;

class AlternatifController extends Controller
{
    public function index()
    {
        $alternatifs = Alternatif::paginate(15);
        $data =[
            'alternatifs' => $alternatifs
        ];
        return View::make('alternatifs.index', $data);
    }

    public function create()
    {
        // $skalas = Skala::with('kriteria')->orderBy('id', 'asc')->get();
        $kriterias = Kriteria::with('skalas')->orderBy('id', 'asc')->get();
        $data = [
            'kriterias' => $kriterias
        ];
        // return $data;
        return view('alternatifs.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "nama_alternatif" => "required|string",
            "kode_alternatif" => "required|string|unique:alternatif,kode_alternatif"
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $alternatif = Alternatif::create([
            "kode_alternatif" => $request->kode_alternatif,
            "nama_alternatif" => $request->nama_alternatif
        ]);

        $users = User::whereIn('role_id', [1, 3])->get();
        for ($i=0; $i <count($users); $i++) {
            // tambahkan baris baru di tabel borda
            $borda = [];
            $borda[] = [
                'rank'          => 0,
                'user_id'       => $users[$i]->id,
                'alternatif_id' => $alternatif->id,
                'spk_group_id'  => Session::get('currentSpkGroup')->id,
                'created_at'    => now(),
                'updated_at'    => now()
            ];
            DB::table('borda')->insert($borda);

            $penilaians = [];
            for ($j=0; $j <count($request->kriteriasIds); $j++) { 
                $penilaians[] = [
                    'kriteria_id'   => $request->kriteriasIds[$j],
                    'alternatif_id' => $alternatif->id,
                    'nilai'         => 0,
                    'spk_group_id'  => Session::get('currentSpkGroup')->id,
                    'user_id'       => $users[$i]->id,
                    'skala_id'      => $request->skalasIds[$j],
                    'created_at'    => now(),
                    'updated_at'    => now()
                ];
            }
            DB::table('penilaian')->insert($penilaians);
        }
        
        $request->session()->flash('status', 'success');
        $request->session()->flash('pesan', 'Alternatif baru berhasil ditambahkan!');
        return response()->json([
            "redirect" => route('alternatif.index')
        ], 201);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Alternatif  $alternatif
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $alternatif = Alternatif::find($id);
        $penilaians = Penilaian::where([
            ['user_id', Session::get('userInfo')->id],
            ['alternatif_id', $alternatif->id]
            ])->get();
        $kriterias = Kriteria::with('skalas')->orderBy('id', 'asc')->get();
        $data = [
            'alternatif' => $alternatif,
            'penilaians' => $penilaians,
            'kriterias' => $kriterias
        ];
        // return $data;
        return view('alternatifs.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Alternatif  $alternatif
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $key)
    {
        // $alternatif = Alternatif::find($key)->with('penilaians')->first();
        // return $alternatif;
        $validator = Validator::make($request->all(), [
            "nama_alternatif" => "required|string",
            "kode_alternatif" => "required|string"
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        $alternatif = Alternatif::find($key);
        if (!$alternatif) {
            Session::flash('status', 'danger');
            Session::flash('pesan', 'Alternatif tidak ditemukan.');
            return redirect()->back();
        }
        $alternatif->kode_alternatif = $request->kode_alternatif;
        $alternatif->nama_alternatif = $request->nama_alternatif;

        $queryPenilaians = Penilaian::where([
            ['user_id', Session::get('userInfo')->id],
            ['alternatif_id', $alternatif->id]
            ])->get();

        $penilaians = [];
        for ($i=0; $i < count($request->kriteriasIds); $i++) { 
            $penilaians[] = [
                'kriteria_id' => $request->kriteriasIds[$i],
                'alternatif_id' => $alternatif->id,
                'nilai' => 0,
                'user_id' => Session::get('userInfo')->id,
                'skala_id' => $request->skalasIds[$i],
                'created_at' => now(),
                'updated_at' => now()
            ];
        }

        // $tPenilaians = Penilaian::where('user_id', Session::get('userInfo')->id)->get();
        for ($i=0; $i < count($penilaians); $i++) { 
            DB::table('penilaian')
                ->where('id', $queryPenilaians[$i]->id)
                ->update($penilaians[$i]);
        }
        $alternatif->save();
        $request->session()->flash('status', 'success');
        $request->session()->flash('pesan', 'Penilaian alternatif berhasil diperbarui!');
        return response()->json([
            "redirect" => route('alternatif.index')
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Alternatif  $alternatif
     * @return \Illuminate\Http\Response
     */
    public function destroy($key)
    {
        $alternatif = Alternatif::find($key);
        if (!$alternatif) {
            Session::flash('status', 'danger');
            Session::flash('pesan', 'Alternatif tidak ditemukan.');
            return redirect()->back();
        }
        $alternatif->delete();
        Session::flash('status', 'success');
        Session::flash('pesan', 'Alternatif berhasil dihapus.');
        return redirect()->route('alternatif.index');
    }

    // DONT USE THIS METHOD, TOO COMPLEX
    // public function import(Request $request){
    //     // Validasi
    //     $validator = Validator::make($request->all(), [
    //         'file_import' => 'required|mimes:xls,xlsx'
    //     ]);

    //     if ($validator->fails()) {
    //         Session::flash('status', 'danger');
    //         Session::flash('pesan', 'Gagal impor data alternatif.'. $validator->errors()->file_import[0]);
    //         return redirect()->back()->withErrors($validator->errors());
    //     }else{
    //         // Menangkap file excel
    //         $file = $request->file('file_import');
    //         // Membuat nama file unik
    //         $nama_file = rand().$file->getClientOriginalName();
    //         // Uplod ke folder /uploads/file_import di dalam folder public 
    //         // Instead of using $file->move(public_path().'/file_cuti/', $fileName);
    //         // use $file->storeAs(public_path().'/file_cuti/', $fileName);
    //         // $path = $request->file('banner_path')->storeAs('public/topic/banner_path', $fileNameToStore);
    //         if($newFilePath = $request->file('file_import')->storeAs('/alternatif/file_import/', $nama_file)){
    //             // $file->move('uploads/file_import', $nama_file);
    //             // Import data
    //             try {
    //                 Excel::import(new AlternatifsImport, storage_path('/app/alternatif/file_import/'. $nama_file));
    //             } catch (Exception $e) {
    //                 unlink(storage_path('/app/alternatif/file_import/'. $nama_file));
    //                 Session::flash('status', 'danger');
    //                 Session::flash('pesan', 'Gagal impor data. '. $e->getMessage());
    //                 return redirect()->route('alternatif.index');
    //             }
    //             // Menghapus file yang sudah diproses
    //             unlink(storage_path('/app/alternatif/file_import/'. $nama_file));
    //             // notifikasi dengan session
    //             Session::flash('status', 'success');
    //             Session::flash('pesan', 'Data alternatif berhasil diimpor.');
    //             // alihkan halaman kembali
    //             return redirect()->route('alternatif.index');
    //         }else{
    //             // notifikasi dengan session
    //             Session::flash('status', 'danger');
    //             Session::flash('pesan', 'Gagal impor data.');
    //             // alihkan halaman kembali
    //             return redirect()->route('alternatif.index');
    //         }
    //     }
    // }
}
