<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\SpkGroup;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Illuminate\Http\Request; 
use Auth;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/spk';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request){
        $rules = [
            'email' => 'required|email',
            'password' => 'required|min:5',
        ];

        $this->validate($request, $rules);

        // attempting login
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $user = User::where('email', $request->email)->first();
            Session::put ('userInfo', $user);
            $spkGroup = SpkGroup::latest()->first();
            if($spkGroup){
                Session::put ('currentSpkGroup', $spkGroup);
            }
            return redirect()->intended(route("spk.index"));
        }
        // Session::put ('currentSpkGroup', '')
        // jika gagal login, maka redirect kembali dengan inputannya
        return redirect()->back()->withInput($request->only(['email', 'remember_token']))
                                ->withErrors(['credentials' => 'Gagal Login, mohon periksa email atau password.']);
    }

    public function logout(Request $request){
        $request->session()->flush();
	    return redirect()->route('login');
    }
}
