<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App\Kriteria;

class KriteriaAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $kriterias = Kriteria::all();
        if(count($kriterias) <= 0) {
            // $request->session()->flash('status', 'warning.');
            // $request->session()->flash('status', 'Anda harus membuat kriteria terlebih dahulu.');
            $request->session()->flash('status', 'Anda harus membuat kriteria terlebih dahulu.');
            return redirect()->route('kriteria.index');
        }
        return $next($request);
    }
}
