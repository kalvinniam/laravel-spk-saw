<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class SpkAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Session::get('currentSpkGroup')) {
            $request->session()->flash('notification', 'Anda harus membuat kelompok perhitungan terlebih dahulu.');
            return redirect()->route('spk.index');
        }
        return $next($request);
    }
}
