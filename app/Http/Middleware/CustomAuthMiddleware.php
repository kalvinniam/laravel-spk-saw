<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class CustomAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // this middleware is for get user id and any information that we can use later
        if(!Session::get('userInfo')) {
            return redirect()->route('login');
        }
        return $next($request);
    }
}
