<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class IsMahasiswa
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::get('userInfo')->role_id == 2){
            Session::flash('type', 'danger');
            Session::flash('notification', 'Gagal memproses. Maaf, Anda bukan seorang Admin atau Dosen.');
            return redirect(route('spk.index'));
        }
        return $next($request);
    }
}
