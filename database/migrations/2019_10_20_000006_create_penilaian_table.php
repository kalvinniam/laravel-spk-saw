<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenilaianTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'penilaian';

    /**
     * Run the migrations.
     * @table penilaian
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->double('nilai')->default('0');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('spk_group_id');
            $table->unsignedInteger('skala_id');
            $table->unsignedInteger('alternatif_id');
            $table->unsignedInteger('kriteria_id');

            $table->index(["user_id"], 'fk_penilaian_user1_idx');

            $table->index(["spk_group_id"], 'fk_penilaian_spk_group1_idx');

            $table->index(["alternatif_id"], 'fk_penilaian_alternatif1_idx');

            $table->index(["skala_id"], 'fk_penilaian_skala1_idx');

            $table->index(["kriteria_id"], 'fk_penilaian_kriteria1_idx');
            $table->nullableTimestamps();

            $table->foreign('spk_group_id', 'fk_penilaian_spk_group1_idx')
                ->references('id')->on('spk_group')
                ->onDelete('cascade');

            $table->foreign('kriteria_id', 'fk_penilaian_kriteria1_idx')
                ->references('id')->on('kriteria')
                ->onDelete('cascade');

            $table->foreign('skala_id', 'fk_penilaian_skala1_idx')
                ->references('id')->on('skala')
                ->onDelete('cascade');

            $table->foreign('user_id', 'fk_penilaian_user1_idx')
                ->references('id')->on('user')
                ->onDelete('cascade');

            $table->foreign('alternatif_id', 'fk_penilaian_alternatif1_idx')
                ->references('id')->on('alternatif')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
