<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpkGroupTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'spk_group';

    /**
     * Run the migrations.
     * @table spk_group
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('kode', 10);
            $table->string('judul', 100);
            $table->unsignedInteger('user_id');

            $table->index(["user_id"], 'fk_spk_group_user1_idx');

            $table->unique(["kode"], 'kode_UNIQUE');
            $table->nullableTimestamps();

            $table->foreign('user_id', 'fk_spk_group_user1_idx')
                ->references('id')->on('user')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
