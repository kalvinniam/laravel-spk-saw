<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkalaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'skala';

    /**
     * Run the migrations.
     * @table skala
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nama_skala', 50);
            $table->string('keterangan', 50);
            $table->integer('value');
            $table->unsignedInteger('kriteria_id');

            $table->index(["kriteria_id"], 'fk_skala_kriteria_idx');
            $table->nullableTimestamps();


            $table->foreign('kriteria_id', 'fk_skala_kriteria_idx')
                ->references('id')->on('kriteria')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
