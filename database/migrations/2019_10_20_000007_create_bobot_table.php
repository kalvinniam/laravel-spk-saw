<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBobotTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'bobot';

    /**
     * Run the migrations.
     * @table bobot
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->double('persentase')->default('0');
            $table->enum('jenis', ['COST', 'BENEFIT'])->default('BENEFIT');
            $table->unsignedInteger('spk_group_id');
            $table->unsignedInteger('kriteria_id');

            $table->index(["spk_group_id"], 'fk_bobot_spk_group1_idx');

            $table->index(["kriteria_id"], 'fk_bobot_kriteria1_idx');
            $table->nullableTimestamps();


            $table->foreign('kriteria_id', 'fk_bobot_kriteria1_idx')
                ->references('id')->on('kriteria')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('spk_group_id', 'fk_bobot_spk_group1_idx')
                ->references('id')->on('spk_group')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
