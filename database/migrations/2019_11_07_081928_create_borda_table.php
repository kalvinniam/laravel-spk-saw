<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBordaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'borda';

    /**
     * Run the migrations.
     * @table borda
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('rank');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('alternatif_id');
            $table->unsignedInteger('spk_group_id');
            $table->nullableTimestamps();

            $table->index(["user_id"], 'fk_borda_user1_idx');
            $table->index(["alternatif_id"], 'fk_borda_alternatif1_idx');
            $table->index(["spk_group_id"], 'fk_borda_spk_group1_idx');

            $table->foreign('user_id', 'fk_borda_user1_idx')
                ->references('id')->on('user')
                ->onDelete('cascade');

            $table->foreign('alternatif_id', 'fk_borda_alternatif1_idx')
                ->references('id')->on('alternatif')
                ->onDelete('cascade');

            $table->foreign('spk_group_id', 'fk_borda_spk_group1_idx')
                ->references('id')->on('spk_group')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->tableName);
    }
}
