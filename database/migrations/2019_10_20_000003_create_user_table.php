<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'user';

    /**
     * Run the migrations.
     * @table user
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nama', 45);
            $table->string('email', 191);
            $table->timestamp('email_verified_at')->nullable()->default(null);
            $table->string('password', 80);
            $table->rememberToken();
            $table->unsignedInteger('role_id');

            $table->index(["role_id"], 'fk_user_role1_idx');

            $table->unique(["email"], 'email_UNIQUE');
            $table->nullableTimestamps();


            $table->foreign('role_id', 'fk_user_role1_idx')
                ->references('id')->on('role')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
