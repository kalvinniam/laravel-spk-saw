<?php

use Illuminate\Database\Seeder;

class SkalaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('skala')->insert([
            [
                'nama_skala' => 'Sangat Baik',
                'value' => 90,
                'keterangan' => '90-100',
                'kriteria_id' => 1,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'nama_skala' => 'Baik',
                'value' => 80,
                'keterangan' => '80-89',
                'kriteria_id' => 1,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'nama_skala' => 'Cukup',
                'value' => 70,
                'keterangan' => '70-79',
                'kriteria_id' => 1,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'nama_skala' => 'Kurang',
                'value' => 60,
                'keterangan' => '60-69',
                'kriteria_id' => 1,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'nama_skala' => 'Sangat Kurang',
                'value' => 50,
                'keterangan' => '50-59',
                'kriteria_id' => 1,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'nama_skala' => 'Sangat Baik',
                'value' => 90,
                'keterangan' => '90-100',
                'kriteria_id' => 2,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'nama_skala' => 'Baik',
                'value' => 80,
                'keterangan' => '80-89',
                'kriteria_id' => 2,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'nama_skala' => 'Cukup',
                'value' => 70,
                'keterangan' => '70-79',
                'kriteria_id' => 2,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'nama_skala' => 'Kurang',
                'value' => 60,
                'keterangan' => '60-69',
                'kriteria_id' => 2,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'nama_skala' => 'Sangat Kurang',
                'value' => 50,
                'keterangan' => '50-59',
                'kriteria_id' => 2,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'nama_skala' => 'Sangat Baik',
                'value' => 90,
                'keterangan' => '90-100',
                'kriteria_id' => 3,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'nama_skala' => 'Baik',
                'value' => 80,
                'keterangan' => '80-89',
                'kriteria_id' => 3,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'nama_skala' => 'Cukup',
                'value' => 70,
                'keterangan' => '70-79',
                'kriteria_id' => 3,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'nama_skala' => 'Kurang',
                'value' => 60,
                'keterangan' => '60-69',
                'kriteria_id' => 3,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'nama_skala' => 'Sangat Kurang',
                'value' => 50,
                'keterangan' => '50-59',
                'kriteria_id' => 3,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'nama_skala' => 'Sangat Baik',
                'value' => 90,
                'keterangan' => '90-100',
                'kriteria_id' => 4,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'nama_skala' => 'Baik',
                'value' => 80,
                'keterangan' => '80-89',
                'kriteria_id' => 4,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'nama_skala' => 'Cukup',
                'value' => 70,
                'keterangan' => '70-79',
                'kriteria_id' => 4,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'nama_skala' => 'Kurang',
                'value' => 60,
                'keterangan' => '60-69',
                'kriteria_id' => 4,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'nama_skala' => 'Sangat Kurang',
                'value' => 50,
                'keterangan' => '50-59',
                'kriteria_id' => 4,
                'created_at' => now(),
                'updated_at' => now()
            ],
        ]);
    }
}
