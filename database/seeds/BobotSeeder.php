<?php

use Illuminate\Database\Seeder;

class BobotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('bobot')->insert([
            [
                'kriteria_id' => 1,
                'persentase' => 0.4,
                'jenis' => 'BENEFIT',
                'spk_group_id' => 1,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'kriteria_id' => 2,
                'persentase' => 0.25,
                'jenis' => 'BENEFIT',
                'spk_group_id' => 1,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'kriteria_id' => 3,
                'persentase' => 0.2,
                'jenis' => 'BENEFIT',
                'spk_group_id' => 1,
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'kriteria_id' => 4,
                'persentase' => 0.15,
                'jenis' => 'BENEFIT',
                'spk_group_id' => 1,
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
