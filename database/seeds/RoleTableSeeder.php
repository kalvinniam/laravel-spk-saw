<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role')->insert(
        [
            [
            'nama_role' => 'DOSEN',
            'created_at' => now(),
            'updated_at' => now()
            ],
            [
                'nama_role' => 'MAHASISWA',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'nama_role' => 'ADMIN',
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
