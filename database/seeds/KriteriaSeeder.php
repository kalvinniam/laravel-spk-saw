<?php

use Illuminate\Database\Seeder;

class KriteriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kriteria')->insert([
            [
                'kode_kriteria' => 'K1',
                'nama_kriteria' => 'Akademik',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'kode_kriteria' => 'K2',
                'nama_kriteria' => 'Keahlian',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'kode_kriteria' => 'K3',
                'nama_kriteria' => 'Kepribadian',
                'created_at' => now(),
                'updated_at' => now()
            ],
            [
                'kode_kriteria' => 'K4',
                'nama_kriteria' => 'Pengalaman',
                'created_at' => now(),
                'updated_at' => now()
            ]
        ]);
    }
}
