<?php

use Illuminate\Database\Seeder;
use App\Alternatif;
use App\User;

class AlternatifTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $alternatif = Alternatif::create([
            "kode_alternatif" => 'KN1',
            "nama_alternatif" => 'Kalvin Niam',
        ]);
        $kriteriasIds = [1,2,3,4];
        $skalasIds = [1, 6, 11, 16];
        $spk_group_id = 1;
        $users = [1,2];
        for ($i=0; $i < count($users); $i++) { 
            $borda = [];
            $borda[] = [
                'rank'          => 0,
                'user_id'       => $users[$i],
                'alternatif_id' => $alternatif->id,
                'spk_group_id'  => $spk_group_id,
                'created_at'    => now(),
                'updated_at'    => now()
            ];
            DB::table('borda')->insert($borda);
            $this->buatPenilaian($kriteriasIds, $alternatif->id, $skalasIds, $users[$i], $spk_group_id);
        }

        $alternatif = Alternatif::create([
            "kode_alternatif" => 'GD1',
            "nama_alternatif" => 'Ghfari Dwi',
        ]);
        $kriteriasIds = [1,2,3,4];
        $skalasIds = [1, 6, 11, 16];
        $spk_group_id = 1;
        $users = [1,2];
        for ($i=0; $i < count($users); $i++) { 
            $borda = [];
            $borda[] = [
                'rank'          => 0,
                'user_id'       => $users[$i],
                'alternatif_id' => $alternatif->id,
                'spk_group_id'  => $spk_group_id,
                'created_at'    => now(),
                'updated_at'    => now()
            ];
            DB::table('borda')->insert($borda);
            $this->buatPenilaian($kriteriasIds, $alternatif->id, $skalasIds, $users[$i], $spk_group_id);
        }

        $alternatif = Alternatif::create([
            "kode_alternatif" => 'FY1',
            "nama_alternatif" => 'Fayyadh Qoimul',
        ]);
        $kriteriasIds = [1,2,3,4];
        $skalasIds = [1, 6, 11, 16];
        $spk_group_id = 1;
        $users = [1,2];
        for ($i=0; $i < count($users); $i++) { 
            $borda = [];
            $borda[] = [
                'rank'          => 0,
                'user_id'       => $users[$i],
                'alternatif_id' => $alternatif->id,
                'spk_group_id'  => $spk_group_id,
                'created_at'    => now(),
                'updated_at'    => now()
            ];
            DB::table('borda')->insert($borda);
            $this->buatPenilaian($kriteriasIds, $alternatif->id, $skalasIds, $users[$i], $spk_group_id);
        }

        $alternatif = Alternatif::create([
            "kode_alternatif" => 'AG1',
            "nama_alternatif" => 'Agus Cahyo',
        ]);
        $kriteriasIds = [1,2,3,4];
        $skalasIds = [1, 6, 11, 16];
        $spk_group_id = 1;
        $users = [1,2];
        for ($i=0; $i < count($users); $i++) { 
            $borda = [];
            $borda[] = [
                'rank'          => 0,
                'user_id'       => $users[$i],
                'alternatif_id' => $alternatif->id,
                'spk_group_id'  => $spk_group_id,
                'created_at'    => now(),
                'updated_at'    => now()
            ];
            DB::table('borda')->insert($borda);
            $this->buatPenilaian($kriteriasIds, $alternatif->id, $skalasIds, $users[$i], $spk_group_id);
        }
    }

    public function buatPenilaian($kriteriasIds, $alternatif_id, $skalasIds, $users_id, $spk_group_id){
        $penilaians = [];
        for ($j=0; $j <count($kriteriasIds); $j++) { 
            $penilaians[] = [
                'kriteria_id' => $kriteriasIds[$j],
                'alternatif_id' => $alternatif_id,
                'nilai' => 0,
                'spk_group_id' => $spk_group_id,
                'user_id' => $users_id,
                'skala_id' => $skalasIds[$j],
                'created_at' => now(),
                'updated_at' => now()
            ];
        }
        DB::table('penilaian')->insert($penilaians);
    }
}
