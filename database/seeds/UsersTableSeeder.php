<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user')->insert([
        [
            'nama' => 'Admin ganteng',
            'email' => 'admin@user.com',
            'email_verified_at' => now(),
            'password' => Hash::make('secret'),
            'role_id' => '3',
            'created_at' => now(),
            'updated_at' => now()
        ],
        [
            'nama' => 'Dosen baik',
            'email' => 'dosen@user.com',
            'email_verified_at' => now(),
            'password' => Hash::make('secret'),
            'role_id' => '1',
            'created_at' => now(),
            'updated_at' => now()
        ],
        [
            'nama' => 'Mahasiswa cakep',
            'email' => 'mahasiswa@user.com',
            'email_verified_at' => now(),
            'password' => Hash::make('secret'),
            'role_id' => '2',
            'created_at' => now(),
            'updated_at' => now()
        ]]
    );
    }
}
