<?php

use Illuminate\Database\Seeder;

class SpkGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('spk_group')->insert(
        [
            [
            'user_id' => 1,
            'kode' => 'SPK1',
            'judul' => 'SPK DOT',
            'created_at' => now(),
            'updated_at' => now()
            ]
        ]);
    }
}
