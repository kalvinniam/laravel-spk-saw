@extends('layouts.app', ['activePage' => 'kriteria', 'titlePage' => 'Manajemen Kriteria'])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('kriteria.store') }}" autocomplete="off" class="form-horizontal">
            @csrf
            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">Tambah Kriteria</h4>
                <p class="card-category"></p>
              </div>
              <div class="card-body ">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="{{ route('kriteria.index') }}" class="btn btn-sm btn-primary">Kembali</a>
                  </div>
                </div>
                <div class="row">
                <label class="col-sm-2 col-form-label text-dark">Kode Kriteria</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('kode_kriteria') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('kode_kriteria') ? ' is-invalid' : '' }}" name="kode_kriteria" id="kode_kriteria" type="text" placeholder="Contoh, K1" value="{{ old('kode_kriteria') }}" required="true" aria-required="true"/>
                      @if ($errors->has('kode_kriteria'))
                        <span id="name-error" class="error text-danger" for="kode_kriteria">{{ $errors->first('kode_kriteria') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                <label class="col-sm-2 col-form-label text-dark">Nama Kriteria</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('nama_kriteria') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('nama_kriteria') ? ' is-invalid' : '' }}" name="nama_kriteria" id="nama_kriteria" type="text" placeholder="Contoh, Sikap" value="{{ old('nama_kriteria') }}" required="true" aria-required="true"/>
                      @if ($errors->has('nama_kriteria'))
                        <span id="name-error" class="error text-danger" for="nama_kriteria">{{ $errors->first('nama_kriteria') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                <label class="col-sm-2 col-form-label text-dark">Bobot Kriteria</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('bobot_kriteria') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('bobot_kriteria') ? ' is-invalid' : '' }}" name="bobot_kriteria" id="bobot_kriteria" type="text" placeholder="Contoh, 0.3" value="{{ old('bobot_kriteria') }}" required="true" aria-required="true"/>
                      @if ($errors->has('bobot_kriteria'))
                        <span id="name-error" class="error text-danger" for="bobot_kriteria">{{ $errors->first('bobot_kriteria') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row mt-3">
                <label class="col-sm-2 col-form-label text-dark">Jenis Kriteria</label>
                  <div class="col-sm-7">
                    <select name="jenis_kriteria" class="custom-select">
                      <option value="BENEFIT">BENEFIT</option>
                      <option value="COST">COST</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">Tambah Kriteria</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection