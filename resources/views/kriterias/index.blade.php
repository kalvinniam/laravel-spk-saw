@extends('layouts.app', ['activePage' => 'kriteria', 'titlePage' => 'Kriteria'])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Daftar Kriteria dan Pembobotan</h4>
              <p class="card-category">Manajemen data kriteria dan pembobotan</p>
            </div>
            <div class="card-body">
              @if (session('status'))
                <div class="row">
                  <div class="col-sm-12">
                    <div class="alert alert-success">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="material-icons">close</i>
                      </button>
                      <span>{{ session('status') }}</span>
                    </div>
                  </div>
                </div>
              @endif
              <div class="row">
                <div class="col-12 text-right">
                  @if(Session::get('userInfo')->role_id == 3)
                    <a href="{{ route('kriteria.create') }}" class="btn btn-sm btn-primary">{{ 'Tambah Kriteria' }}</a>
                  @endif
                </div>
              </div>
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>
                      No.
                    </th>
                    <th>
                      Kode Kriteria
                    </th>
                    <th>
                      Nama Kriteria
                    </th>
                    <th>
                      Jenis Kriteria
                    </th>
                    <th>
                      Bobot Kriteria
                    </th>
                    @if(Session::get('userInfo')->role_id == 3)
                      <th class="text-right">
                        Opsi
                      </th>
                    @endif
                  </thead>
                  <tbody>
                    @foreach($kriterias as $baris)
                      <tr>
                        <td>
                          {{ $loop->iteration }}
                        </td>
                        <td>
                            {{ $baris->kriteria->kode_kriteria }}
                        </td>
                        <td>
                            {{ $baris->kriteria->nama_kriteria }}
                        </td>
                        <td>
                            {{ $baris->jenis }}
                        </td>
                        <td>
                            {{ $baris->persentase }}
                        </td>
                        @if(Session::get('userInfo')->role_id == 3)
                          <td class="td-actions text-right">
                            <form action="{{ route('kriteria.destroy', ['id'=> $baris->kriteria->id]) }}" method="post">
                              @csrf
                              @method('delete')
                              <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('kriteria.edit', $baris->id)  }}" data-original-title="" title="">
                                <i class="material-icons">edit</i>
                                <div class="ripple-container"></div>
                              </a>
                              <button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="confirm('Apakah Anda yakin ingin menghapus kriteria ini?') ? this.parentElement.submit() : ''">
                                  <i class="material-icons">close</i>
                                  <div class="ripple-container"></div>
                              </button>
                            </form>
                          </td>
                        @endif
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection