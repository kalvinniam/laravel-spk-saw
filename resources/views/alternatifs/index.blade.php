@extends('layouts.app', ['activePage' => 'alternatif', 'titlePage' => 'Alternatif'])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Daftar Alternatif dan Pembobotan</h4>
              <p class="card-category">Manajemen data alternatif dan penilaian</p>
            </div>
            <div class="card-body">
              @if (session('pesan'))
                <div class="row">
                  <div class="col-sm-12">
                    <div class="alert alert-{{ session('status') }}">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="material-icons">close</i>
                      </button>
                      <span>{{ session('pesan') }}</span>
                    </div>
                  </div>
                </div>
              @endif
              {{-- Modal --}}
              {{-- <div id="modalImport" class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <form action="{{route('alternatif.import')}}" method="post" enctype="multipart/form-data" >
                      <div class="modal-header">
                        <h5 class="modal-title">Impor Data Alternatif</h5>
                      </div>
                      <div class="modal-body">
                        <div class="col-md-12 mb-4">
                            {{ csrf_field() }}
                            <input type="file" name="file_import" id="file_import">
                        </div>
                        <p class="text-warning">NB:</p>
                        <p class="text-warning">- Pastikan Kelengkapan data sebelum upload file.</p>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-primary" type="submit">Upload</button>
                        <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div> --}}
              {{-- End Modal --}}
              <div class="row">
                {{-- <a href="{{ route('alternatif.import') }}" class="btn btn-sm btn-primary">{{ 'Import Alternatif' }}</a> --}}
                @if(Session::get('userInfo')->role_id == 1 || Session::get('userInfo')->role_id == 3)
                  {{-- <div class="col-6 text-left">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalImport">
                      Impor Excel
                    </button>
                  </div> --}}
                  <div class="col-12 text-right">
                    <a href="{{ route('alternatif.create') }}" class="btn btn-sm btn-primary">{{ 'Tambah Alternatif' }}</a>
                  </div>
                  @endif
              </div>
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>
                      No.
                    </th>
                    <th>
                      Kode Alternatif
                    </th>
                    <th>
                      Nama Alternatif
                    </th>
                    <th>
                      Dibuat pada
                    </th>
                    <th>
                      Diperbarui pada
                    </th>
                    @if(Session::get('userInfo')->role_id == 1 || Session::get('userInfo')->role_id == 3)
                      <th class="text-right">
                        Opsi
                      </th>
                    @endif
                  </thead>
                  <tbody>
                    @foreach($alternatifs as $alternatif)
                      <tr>
                        <td>
                          {{ $loop->iteration }}
                        </td>
                        <td>
                            {{ $alternatif->kode_alternatif }}
                        </td>
                        <td>
                            {{ $alternatif->nama_alternatif }}
                        </td>
                        <td>
                            {{ $alternatif->created_at }}
                        </td>
                        <td>
                            {{ $alternatif->updated_at }}
                        </td>
                        @if(Session::get('userInfo')->role_id == 1 || Session::get('userInfo')->role_id == 3)
                          <td class="td-actions text-right">
                              <form action="{{ route('alternatif.destroy', ['id'=> $alternatif->id]) }}" method="post">
                                @csrf
                                @method('delete')
                                <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('alternatif.edit', $alternatif->id)  }}" data-original-title="" title="">
                                  <i class="material-icons">edit</i>
                                  <div class="ripple-container"></div>
                                </a>
                                <button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="confirm('Apakah Anda yakin ingin menghapus alternatif ini?') ? this.parentElement.submit() : ''">
                                    <i class="material-icons">close</i>
                                    <div class="ripple-container"></div>
                                </button>
                              </form>
                            </td>
                          @endif
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection