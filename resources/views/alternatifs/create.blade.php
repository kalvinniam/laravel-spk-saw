@extends('layouts.app', ['activePage' => 'alternatif', 'titlePage' => 'Manajemen Alternatif'])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route("alternatif.store") }}" autocomplete="off" class="form-horizontal">
            @csrf
            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">Tambah Alternatif</h4>
                <p class="card-category"></p>
              </div>
              <div class="card-body ">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="{{ route('alternatif.index') }}" class="btn btn-sm btn-primary">Kembali</a>
                  </div>
                </div>
                <div class="row">
                <label class="col-sm-2 col-form-label text-dark">Kode Alternatif</label>
                  <div class="col-sm-7">
                    <div id="fgkode_alternatif" class="form-group{{ $errors->has('kode_alternatif') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('kode_alternatif') ? ' is-invalid' : '' }}" name="kode_alternatif" id="kode_alternatif" type="text" placeholder="Contoh, AL1" value="{{ old('kode_alternatif') }}" required="true" aria-required="true"/>
                      @if ($errors->has('kode_alternatif'))
                        <span id="name-error" class="error text-danger" for="kode_alternatif">{{ $errors->first('kode_alternatif') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                <label class="col-sm-2 col-form-label text-dark">Nama Alternatif</label>
                  <div class="col-sm-7">
                    <div id="fgnama_alternatif" class="form-group{{ $errors->has('nama_alternatif') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('nama_alternatif') ? ' is-invalid' : '' }}" name="nama_alternatif" id="nama_alternatif" type="text" placeholder="Contoh, Kalvin Niam" value="{{ old('nama_alternatif') }}" required="true" aria-required="true"/>
                      @if ($errors->has('nama_alternatif'))
                        <span id="name-error" class="error text-danger" for="nama_alternatif">{{ $errors->first('nama_alternatif') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                @foreach ($kriterias as $kriteria)
                  <div id="penilaianKriteria" class="row mt-3">
                    <label class="col-sm-2 col-form-label text-dark">{{$kriteria->nama_kriteria}}</label>
                    <div class="col-sm-7">
                      <select name="{{$kriteria->id}}" class="custom-select">
                        @foreach ($kriteria->skalas as $skala)
                          <option value="{{$skala->id}}">{{$skala->nama_skala}} - {{$skala->keterangan}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                @endforeach
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button id="submitBtn" type="button" class="btn btn-primary">Tambah Alternatif</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection
@push('js')
  <script>
    $(document).ready(function() {
      $("#submitBtn").on('click', function(){
        $(this).attr('disabled', true)
          const kode_alternatif = $('input[name="kode_alternatif"]').val(); 
          const nama_alternatif = $('input[name="nama_alternatif"]').val(); 
          let kriteriasIds = []
          let skalasIds = []

          const select = $("#penilaianKriteria select").map( (k, v)=>{
            kriteriasIds.push($(v).attr('name'))
            skalasIds.push($(v).val())
          })

          console.log(kriteriasIds)
          console.log(skalasIds)
          let data = { 
              kode_alternatif:kode_alternatif, 
              nama_alternatif:nama_alternatif ,
              kriteriasIds:kriteriasIds,
              skalasIds:skalasIds
            }
            $.ajax({
              url: '{{route("alternatif.store")}}',
              method: "POST",
              dataType: "json",
              headers: {
                  'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              data: data
          }).done((response) => {
            if(response.redirect){
              $(this).attr('disabled', false)
              window.location.href = response.redirect;
            }else{
              console.log(response)
            }
          }).fail((jqXHR, textStatus) => {
            if(jqXHR.status == 422){
                if(jqXHR.responseJSON.kode_alternatif != undefined){
                  $('#fgkode_alternatif').addClass('has-danger')
                  $('#kode_alternatif').addClass('is-invalid')
                  $('#kode_alternatif').after('<span id="name-error" class="error text-danger" for="kode_alternatif">'+ jqXHR.responseJSON.kode_alternatif +'</span>')
                }
                if(jqXHR.responseJSON.nama_alternatif != undefined){
                  $('#fgnama_alternatif').addClass('has-danger')
                  $('#nama_alternatif').addClass('is-invalid')
                  $('#nama_alternatif').after('<span id="name-error" class="error text-danger" for="nama_alternatif">'+ jqXHR.responseJSON.nama_alternatif +'</span>')
                }
            }else{
                console.log(jqXHR)
                alert( "Maaf, terjadi kesalahan. Error: " + textStatus );
            }
          });
      });
    });
  </script>
@endpush