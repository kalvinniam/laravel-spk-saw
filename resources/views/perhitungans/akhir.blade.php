@extends('layouts.app', ['activePage' => 'perhitungan-akhir', 'titlePage' => 'Hasil Akhir'])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
          <div class="col-sm-12">
            <div class="alert alert-primary">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <i class="material-icons">close</i>
              </button>
              <div style="text-align: justify; width: 100%;">
                <span>Metode Borda merupakan metode voting yang dapat menyelesaikan pengambilan
                    keputusan kelompok, dimana dalam penerapannya masing-masing decision maker memberikan
                    peringkat berdasarkan alternatif pilihan yang ada, proses pemilihan dalam metode Borda,
                    masing-masing voter diberikan alternatif pilihan. Di misalkan ada n kandidat pilihan, kandidat
                    atau alternatif pertama diberikan n poin oleh voter atau decisian maker. Kandidat kedua
                    diberikan poin n-1 dan seterusnnya. Penentuan pemenang atau alternatif terbaik berdasarkan
                    poin yang tertinggi. Alternatif dengan nilai tertinggi merupakan bahan pertimbangan yang akan
                    dipilih. <a href="http://ieeexplore.ieee.org/document/1342832"><u>Wang, C.W.C. & Leung, H.L.H., 2004. A secure and fully private borda voting protocol
                        with universal verifiability. Proceedings of the 28th Annual International Computer
                        Software and Applications Conference, 2004. COMPSAC 2004.</u></a></span>
              </div>
            </div>
          </div>
        </div>
      <div id="spkGroup" class="row align-items-center">
        <div class="col-md-4">Hasil akhir untuk kelompok perhitungan:</div>
        <div class="col-lg-3 col-md-6 col-sm-8">
          <div data-judulspk="{{Session::get('currentSpkGroup')->judul}}" data-idspk="{{Session::get('currentSpkGroup')->id}}" class="card card-stats bg-purple border border-primary">
            <div class="card-header card-header-primary card-header-icon">
              <div class="card-icon" style="z-index: 99;">
                <i class="material-icons">content_copy</i>
              </div>
              <p class="card-category">{{Session::get('currentSpkGroup')->judul}}</p>
              <h3 class="card-title">{{Session::get('currentSpkGroup')->kode}}</h3>
            </div>
            <div class="card-footer">
              <div class="stats">
                <i class="material-icons text-danger">access_time</i>
                <span>{{date('j M Y H:i', strtotime(Session::get('currentSpkGroup')->updated_at))}} WIB</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Tabel Hasil Akhir</h4>
              <p class="card-category">Hasil akhir menyatakan bahwa:</p>
            </div>
            <div class="card-body">
              @if (session('status'))
                <div class="row">
                  <div class="col-sm-12">
                    <div class="alert alert-success">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="material-icons">close</i>
                      </button>
                      <span>{{ session('status') }}</span>
                    </div>
                  </div>
                </div>
              @endif
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead class="text-primary">
                    <th>
                      No.
                    </th>
                    <th>
                      Kode Alternatif
                    </th>
                    <th>
                      Nama Alternatif
                    </th>
                    <th>
                      Jumlah Skor
                    </th>
                  </thead>
                  <tbody>
                    @foreach ($hasils as $hasil)
                      <tr>
                        <td>
                          {{$loop->index + 1}}
                        </td>
                        <td>
                          {{$hasil->kode_alternatif}}
                        </td>
                        <td>
                          {{$hasil->nama_alternatif}}
                        </td>
                        <td>
                          {{$hasil->skor}}
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection