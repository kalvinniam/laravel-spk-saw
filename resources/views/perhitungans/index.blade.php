@extends('layouts.app', ['activePage' => 'perhitungan', 'titlePage' => 'Perhitungan SAW'])

@section('content')
  <div class="content">
    <div class="container-fluid">
      {{-- <div class="row">
        <div class="col-sm-12">
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <i class="material-icons">close</i>
            </button>
            <span>Perhitungan ini dihitung berdasarkan penilaian Anda terhadap setiap alternatif.</span>
          </div>
        </div>
      </div> --}}
      <div id="penilaianKriteria" class="row mt-3">
        <label class="col-sm-3 col-form-label text-dark">Berdasarkan penilaian user:</label>
        <div class="col-sm-7">
          <form action="{{route('perhitungan.index')}}" method="GET">
            <select id="perhitungan_user" name="user" class="custom-select">
              @foreach ($users as $user)
                <option {{$perhitungan_user == $user->id ? 'selected': ''}} value="{{$user->id}}">{{$user->nama}}</option>
              @endforeach
            </select>
          </form>
        </div>
      </div>
      {{-- Hasil Perhitungan Metode SAW --}}
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Tabel Kriteria</h4>
              <p class="card-category">Informasi tentang kriteria yang digunakan dalam perhitungan.</p>
            </div>
            <div class="card-body">
              @if (session('status'))
                <div class="row">
                  <div class="col-sm-12">
                    <div class="alert alert-success">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="material-icons">close</i>
                      </button>
                      <span>{{ session('status') }}</span>
                    </div>
                  </div>
                </div>
              @endif
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead class="text-primary">
                    <th>
                      #
                    </th>
                    @foreach ($pembobotans as $pembobotan)
                      <th>
                        {{$pembobotan->kriteria->nama_kriteria}}
                      </th>
                    @endforeach
                  </thead>
                  <tbody>
                    <tr>
                      <td class="text-primary">COST / BENEFIT</td>
                      @foreach ($pembobotans as $pembobotan)
                      <td>
                        {{$pembobotan->jenis}}
                      </td>
                      @endforeach
                    </tr>
                    <tr>
                      <td class="text-primary">
                        Bobot
                      </td>
                      @foreach ($pembobotans as $pembobotan)
                      <td>
                        {{$pembobotan->persentase}}
                      </td>
                      @endforeach
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Matriks Awal</h4>
              <p class="card-category">Matriks sebelum dilakukan perhitungan</p>
            </div>
            <div class="card-body">
              @if (session('status'))
                <div class="row">
                  <div class="col-sm-12">
                    <div class="alert alert-success">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="material-icons">close</i>
                      </button>
                      <span>{{ session('status') }}</span>
                    </div>
                  </div>
                </div>
              @endif
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead class=" text-primary">
                    <th>
                      Kode Alternatif
                    </th>
                    @foreach ($pembobotans as $pembobotan)
                      <th>
                        {{$pembobotan->kriteria->nama_kriteria}}
                      </th>
                    @endforeach
                  </thead>
                  <tbody>
                    @foreach ($datas as $alternatif)
                      <tr>
                        <td>
                          {{$alternatif->kode_alternatif}}
                        </td>
                        @foreach ($alternatif->penilaians as $nilai)
                          <td>
                            {{$nilai->skala->value}}
                          </td>
                        @endforeach
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Matriks Normalisasi</h4>
              <p class="card-category">Matriks sesudah dilakukan perhitungan menggunakan metode SAW.</p>
            </div>
            <div class="card-body">
              @if (session('status'))
                <div class="row">
                  <div class="col-sm-12">
                    <div class="alert alert-success">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="material-icons">close</i>
                      </button>
                      <span>{{ session('status') }}</span>
                    </div>
                  </div>
                </div>
              @endif
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead class=" text-primary">
                    <th>
                      Kode Alternatif
                    </th>
                    @foreach ($pembobotans as $pembobotan)
                      <th>
                        {{$pembobotan->kriteria->nama_kriteria}}
                      </th>
                    @endforeach
                  </thead>
                  <tbody>
                    @foreach ($datas as $alternatif)
                      <tr>
                        <td>
                          {{$alternatif->kode_alternatif}}
                        </td>
                        @foreach ($matriksNorm[$loop->index] as $nilai)
                          <td>
                            {{$nilai}}
                          </td>
                        @endforeach
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Tabel Hasil Perhitungan SAW</h4>
              <p class="card-category">Analisis hasil perhitungan SAW menyatakan bahwa:</p>
            </div>
            <div class="card-body">
              @if (session('status'))
                <div class="row">
                  <div class="col-sm-12">
                    <div class="alert alert-success">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="material-icons">close</i>
                      </button>
                      <span>{{ session('status') }}</span>
                    </div>
                  </div>
                </div>
              @endif
              <div class="table-responsive">
                <table class="table table-hover">
                  <thead class="text-primary">
                    <th>
                      No.
                    </th>
                    <th>
                      Kode Alternatif
                    </th>
                    <th>
                      Nama Alternatif
                    </th>
                    <th>
                      Jumlah Poin
                    </th>
                  </thead>
                  <tbody>
                    @foreach ($rangkings as $rangking)
                      <tr>
                        <td>
                          {{$loop->index + 1}}
                        </td>
                        <td>
                          {{$rangking->kode_alternatif}}
                        </td>
                        <td>
                          {{$rangking->nama_alternatif}}
                        </td>
                        <td>
                          {{$rangking->poin}}
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      {{-- End Hasil Perhitungan Metode SAW --}}
    </div>
  </div>
@endsection

@push('js')
    <script>
      $('select#perhitungan_user').on('change', function() {
        this.form.submit();
      });
    </script>
@endpush