<footer class="footer">
    <div class="container">
        <nav class="float-left">
        <ul>
            <li>
            <a href="#">
                Tentang Kami
            </a>
        </ul>
        </nav>
        <div class="copyright float-right">
        &copy;
        <script>
            document.write(new Date().getFullYear())
        </script>, made with <i class="material-icons">favorite</i> by <a href="https://instagram.com/niamkvn">vin</a>
        </div>
    </div>
</footer>