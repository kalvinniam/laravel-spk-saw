<div class="sidebar" data-color="purple" data-background-color="white" data-image="{{ asset('material') }}/img/sidebar-1.jpg">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <div class="logo">
    <a href="#" class="simple-text logo-normal">
      {{Session::get('userInfo')->nama}}
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('spk.index') }}">
          <i class="material-icons">dashboard</i>
            <p id="spkSelected">{{ Session::get('currentSpkGroup') ? Session::get('currentSpkGroup')->judul : "-Belum ada perhitungan-"}}</p>
        </a>
      </li>
      <li class="nav-item {{ ($activePage == 'alternatif' || $activePage == 'kritria' || $activePage == 'skala' || $activePage == 'perhitungan') ? ' active' : '' }}">
        <a class="nav-link" data-toggle="collapse" href="#laravelExample" aria-expanded="true">
          <i><img style="width:25px" src="{{ asset('material') }}/img/laravel.svg"></i>
          <p>Data Master
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse show" id="laravelExample">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'alternatif' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('alternatif.index') }}">
                <span class="sidebar-mini"> AL </span>
                <span class="sidebar-normal">Data Alternartif </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'kriteria' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('kriteria.index') }}">
                <span class="sidebar-mini"> KR </span>
                <span class="sidebar-normal"> Data Kriteria </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'skala' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('skala.index') }}">
                <span class="sidebar-mini"> SP </span>
                <span class="sidebar-normal"> Skala Penilaian </span>
              </a>
            </li>
          </ul>
        </div>
      </li>
      @if (Session::get('userInfo')->role_id == 1 || Session::get('userInfo')->role_id == 3)
        <li class="nav-item{{ $activePage == 'perhitungan' ? ' active' : '' }}">
          <a class="nav-link" href="{{ route('perhitungan.index') }}">
            <i class="material-icons">content_paste</i>
              <p>Perhitungan SAW</p>
          </a>
        </li>
      @endif
      <li class="nav-item{{ $activePage == 'perhitungan-akhir' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('perhitungan.akhir') }}">
          <i class="material-icons">insert_chart</i>
            <p>Hasil Akhir</p>
        </a>
      </li>
    </ul>
  </div>
</div>