@extends('layouts.app', ['activePage' => 'dashboard', 'titlePage' => 'Dashboard'])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          @if ($message = Session::get('notification'))
            <div class="alert alert-{{Session::get('type') ? Session::get('type'): 'danger' }} alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
          @endif
        </div>
      </div>
      <div id="spkGroup" class="row">
        @forelse ($spkGroups as $spkGroup)
          <div class="col-lg-3 col-md-6 col-sm-6">
            @if ($spkGroup->kode == Session::get('currentSpkGroup')->kode)
              <div data-judulspk="{{$spkGroup->judul}}" data-idspk="{{$spkGroup->id}}" class="card card-stats bg-purple border border-primary">
                <div class="card-header card-header-primary card-header-icon">
            @else
                <div data-judulspk="{{$spkGroup->judul}}" data-idspk="{{$spkGroup->id}}" class="card card-stats bg-purple">
                <div class="card-header card-header-secondary card-header-icon">
            @endif
                <div class="card-icon" style="z-index: 99;">
                  <i class="material-icons">content_copy</i>
                </div>
                <p class="card-category">{{$spkGroup->judul}}</p>
                <h3 class="card-title">{{$spkGroup->kode}}</h3>
              </div>
              <div class="card-footer">
                <div class="stats">
                  <i class="material-icons text-danger">access_time</i>
                  <span>{{date('j M Y H:i', strtotime($spkGroup->updated_at))}} WIB</span>
                </div>
              </div>
            </div>
          </div>
        @empty
        <div class="col-lg-3 col-md-6 col-sm-6">
          <h3>Belum Ada Kelompok Perhitungan</h3>
        </div>
        @endforelse
      </div>
      {{-- Create SPK --}}
      <div class="row">
        <div class="col-md-12">
          <form id="spkForm" method="post" action="{{ route('spk.store') }}" autocomplete="off" class="form-horizontal">
            @csrf
            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">Buat Kelompok Perhitungan</h4>
                <p class="card-category"></p>
              </div>
              <div class="card-body ">
                <div class="row">
                  @if(Session::get('userInfo')->role_id == 1 || Session::get('userInfo')->role_id == 3)
                    <div class="col-md-10 text-right">
                        <button id="cancelBtn" type="button" class="btn btn-sm btn-primary hidden">Batal Edit</button>
                    </div>
                  @endif
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label text-dark">Kode Kelompok Perhitungan</label>
                  <div class="col-sm-7">
                    <div id="fgkode" class="form-group{{ $errors->has('kode') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('kode') ? ' is-invalid' : '' }}" name="kode" id="kode_spk_group" type="text" placeholder="SPK1" value="{{ old('kode') }}" required="true" aria-required="true"/>
                      @if ($errors->has('kode'))
                        <span id="name-error" class="error text-danger" for="input-kode">{{ $errors->first('kode') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label text-dark">Judul Kelompok Perhitungan</label>
                  <div class="col-sm-7">
                    <div id="fgjudul" class="form-group{{ $errors->has('judul') ? ' has-danger' : '' }}">
                      <input name="user_id" type="hidden" value="{{Session::get('userInfo')->id}}"/>
                      <input class="form-control{{ $errors->has('judul') ? ' is-invalid' : '' }}" name="judul" id="judul_spk_group" type="text" placeholder="PT. Array Suara Gemilang" value="{{ old('judul') }}" required="true" aria-required="true"/>
                      @if ($errors->has('judul'))
                        <span id="name-error" class="error text-danger" for="input-judul">{{ $errors->first('judul') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              @if(Session::get('userInfo')->role_id == 1 || Session::get('userInfo')->role_id == 3)
                <div class="card-footer ml-auto mr-auto">
                  <button id="submitBtn" type="submit" class="btn btn-primary">Buat</button>
                </div>
              @endif
            </div>
          </form>
        </div>
      </div>
      {{-- End Create SPK --}}
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header card-header-primary">
              <h4 class="card-title ">Daftar Kelompok Perhitungan</h4>
              <p class="card-category"></p>
            </div>
            <div class="card-body">
              @if (session('status'))
                <div class="row">
                  <div class="col-sm-12">
                    <div class="alert alert-success">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="material-icons">close</i>
                      </button>
                      <span>{{ session('status') }}</span>
                    </div>
                  </div>
                </div>
              @endif
              <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>
                        ID
                    </th>
                    <th>
                      Kode Kelompok Perhitungan
                    </th>
                    <th>
                      Judul Kelompok Perhitungan
                    </th>
                    <th>
                      Dibuat Pada
                    </th>
                    <th>
                      Diperbarui Pada
                    </th>
                    @if(Session::get('userInfo')->role_id == 1 || Session::get('userInfo')->role_id == 3)
                      <th class="text-right">
                        Opsi
                      </th>
                    @endif
                  </thead>
                  <tbody>
                    @foreach($spkGroups as $spk)
                      <tr>
                        <td>
                          {{ $loop->iteration }}
                        </td>
                        <td>
                            {{ $spk->kode }}
                        </td>
                        <td>
                            {{ $spk->judul }}
                        </td>
                        <td>
                            {{ $spk->created_at }}
                        </td>
                        <td>
                            {{ $spk->updated_at }}
                        </td>
                        @if(Session::get('userInfo')->role_id == 1 || Session::get('userInfo')->role_id == 3)
                          <td class="td-actions text-right">
                            <form action="{{route('spk.destroy', ['id'=> $spk->id])}}" method="post">
                              @csrf
                              @method('delete')
                              <button id="{{$spk->id}}" type="button" rel="tooltip" class="editBtn btn btn-success btn-link" href="" data-original-title="" title="">
                                <i class="material-icons">edit</i>
                                <div class="ripple-container"></div>
                              </button>
                              <button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="confirm('Apakah Anda yakin ingin menghapus kelompok perhitungan {{$spk->judul}} ini?') ? this.parentElement.submit() : ''">
                                <i class="material-icons">close</i>
                                <div class="ripple-container"></div>
                              </button>
                            </form>
                          </td>
                        @endif
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('js')
  <script>
    let editMode = false;
    $(document).ready(function() {
      $("#cancelBtn").hide()
      $("#spkGroup .card").on('click', function() {
        const judulspk = $(this).data('judulspk');
        const idspk = $(this).data('idspk');
        let selected = $(this)
        $.get("/spk/select/" + idspk, function( response ) {
          console.log("selecting " + idspk + ": " + response)
          if(response == "ok"){
            $("#spkSelected").text(judulspk);
            $("#spkGroup .card").removeClass("border border-primary");
            selected.addClass("border border-primary");
            $("#spkGroup .card").find('.card-header').removeClass("card-header-primary");
            selected.find('.card-header').addClass('card-header-primary')
          }
        });
      })
      $("#cancelBtn").on('click', function() {
        $(this).hide()
        $('input[name="kode"]').val("")
        $('input[name="judul"]').val("")
        $('#submitBtn').text("Buat")
        editMode = false
      })
      let currentSpkEdited_id = ""
      $(".editBtn").on('click', function() {
        console.log("editing " +  + ": " + $(this).attr('id'))
        $.get("/spk/edit/" + $(this).attr('id'), function( response ) {
          currentSpkEdited_id = $(this).attr('id')
          $('input[name="kode"]').val(response.kode)
          $('input[name="judul"]').val(response.judul)
          $('#submitBtn').text("Simpan")
          $("#cancelBtn").show()
          editMode = true
        });
      })

      $("#spkForm").submit(function(e){
        if(editMode){
          e.preventDefault()
          $("#submitBtn").attr('disabled', true)
            $.ajax({
              url: currentSpkEdited_id!=""? '/spk/edit/' + currentSpkEdited_id: "#",
              method: "POST",
              dataType: "json",
              headers: {
                  'X-CSRF-TOKEN': '{{ csrf_token() }}'
              },
              data: $(this).serialize()
          }).done((response) => {
            $("#submitBtn").attr('disabled', false)
            if(response.redirect){
              window.location.href = response.redirect;
            }else{
              console.log(response)
            }
            $("#submitBtn").attr('disabled', false)
          }).fail((jqXHR, textStatus) => {
            if(jqXHR.status == 422){
                if(jqXHR.responseJSON.kode != undefined){
                  $('#fgkode').addClass('has-danger')
                  $('#kode').addClass('is-invalid')
                  $('#kode').after('<span id="name-error" class="error text-danger" for="kode">'+ jqXHR.responseJSON.kode +'</span>')
                }
                if(jqXHR.responseJSON.judul != undefined){
                  $('#fgjudul').addClass('has-danger')
                  $('#judul').addClass('is-invalid')
                  $('#judul').after('<span id="name-error" class="error text-danger" for="judul">'+ jqXHR.responseJSON.judul +'</span>')
                }
            }else{
                console.log(jqXHR)
                alert( "Maaf, terjadi kesalahan. Error: " + textStatus );
            }
          });
        }
      });
    });
  </script>
@endpush