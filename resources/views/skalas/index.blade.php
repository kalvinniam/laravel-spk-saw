@extends('layouts.app', ['activePage' => 'skala', 'titlePage' => 'Skala'])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        @forelse ($kriterias as $kriteria)
          <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title ">{{$kriteria->nama_kriteria}}</h4>
                <p class="card-category">Daftar Skala penilaian untuk kriteria '{{$kriteria->nama_kriteria}}'</p>
              </div>
              <div class="card-body">
                @if (session('status') && session('kriteria_id') == $kriteria->id)
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="row">
                  <div class="col-12 text-right">
                    @if(Session::get('userInfo')->role_id == 1 || Session::get('userInfo')->role_id == 3)
                      <a href="{{ route('skala.create', ['kriteria_id'=> $kriteria->id]) }}" class="btn btn-sm btn-primary">Tambah Skala</a>
                    @endif
                  </div>
                </div>
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                      <th>
                        No.
                      </th>
                      <th>
                        Nama Skala
                      </th>
                      <th>
                        Keterangan
                      </th>
                      <th>
                        Nilai (value)
                      </th>
                      @if(Session::get('userInfo')->role_id == 1 || Session::get('userInfo')->role_id == 3)
                        <th class="text-right">
                          Opsi
                        </th>
                      @endif
                    </thead>
                    <tbody>
                      @foreach($kriteria->skalas as $skala)
                        <tr>
                          <td>
                            {{ $loop->iteration }}
                          </td>
                          <td>
                              {{ $skala->nama_skala }}
                          </td>
                          <td>
                              {{ $skala->keterangan }}
                          </td>
                          <td>
                              {{ $skala->value }}
                          </td>
                          @if(Session::get('userInfo')->role_id == 1 || Session::get('userInfo')->role_id == 3)
                            <td class="td-actions text-right">
                              <form action="{{ route('skala.destroy', ['skala_id'=>$skala->id, 'kriteria_id'=>$kriteria->id]) }}" method="post">
                                @csrf
                                @method('delete')
                                <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('skala.edit', ['skala_id'=>$skala->id, 'kriteria_id'=>$kriteria->id])  }}" data-original-title="" title="">
                                  <i class="material-icons">edit</i>
                                  <div class="ripple-container"></div>
                                </a>
                                <button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="confirm('Apakah Anda yakin ingin menghapus skala ini?') ? this.parentElement.submit() : ''">
                                    <i class="material-icons">close</i>
                                    <div class="ripple-container"></div>
                                </button>
                              </form>
                            </td>
                          @endif
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        @empty
          <div class="col-sm-12">
            <div class="alert alert-warning">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <i class="material-icons">close</i>
              </button>
              <span>Belum ada skala kriteria. Anda bisa membuat kriteria terlebih dahulu.</span>
              <a href="{{route('kriteria.create')}}" class="btn btn-success">Buat Kriteria</a>
            </div>
          </div>
        @endforelse
      </div>
    </div>
  </div>
@endsection