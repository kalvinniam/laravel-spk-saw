@extends('layouts.app', ['activePage' => 'skala', 'titlePage' => 'Tambah Skala'])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('skala.update', ['skala_id'=>$skala->id, 'kriteria_id'=>$skala->kriteria->id]) }}" autocomplete="off" class="form-horizontal">
            @csrf
            @method('put')
            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">Edit Skala Penilaian</h4>
                <p class="card-category">Edit skala penilaian untuk kriteria '{{$skala->kriteria->nama_kriteria}}'</p>
              </div>
              <div class="card-body ">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="{{ route('skala.index') }}" class="btn btn-sm btn-primary">Kembali</a>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label text-dark">Nama Skala</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('nama_skala') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('nama_skala') ? ' is-invalid' : '' }}" name="nama_skala" id="nama_skala" type="text" placeholder="Contoh, Sangat Baik" value="{{ $skala->nama_skala }}" required="true" aria-required="true"/>
                      @if ($errors->has('nama_skala'))
                        <span id="name-error" class="error text-danger" for="nama_skala">{{ $errors->first('nama_skala') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label text-dark">Keterangan Skala</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('keterangan') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('keterangan') ? ' is-invalid' : '' }}" name="keterangan" id="keterangan" type="text" placeholder="Contoh, 90-100" value="{{ $skala->keterangan }}" required="true" aria-required="true"/>
                      @if ($errors->has('keterangan'))
                        <span id="name-error" class="error text-danger" for="keterangan">{{ $errors->first('keterangan') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <label class="col-sm-2 col-form-label text-dark">Nilai(value) Skala</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('value') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('value') ? ' is-invalid' : '' }}" name="value" id="value" type="text" placeholder="Contoh, 5" value="{{ $skala->value }}" required="true" aria-required="true"/>
                      @if ($errors->has('value'))
                        <span id="name-error" class="error text-danger" for="value">{{ $errors->first('value') }}</span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">Simpan Skala</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection